#!/usr/bin/env python
# -*-coding: utf8 -*-

from urllib2 import build_opener, HTTPSHandler, Request, HTTPError
import requests
# from urllib import quote as urlquote
# from StringIO import StringIO

# import re, os, time, hmac, base64, hashlib, urllib, mimetypes
# from collections import Iterable
# from datetime import datetime, timedelta, tzinfo

from utils import bytes, _encode_json, _parse_json, JsonObject


class Telegram(object):

    def __init__(self, access_token):
        self._token = access_token

    def __getattr__(self, attr):
        # return _Callable(self, attr)
        def run(**kw):
            return self._http(attr, **kw)
        return run

    def _http(self, _method, **kw):
        url = 'https://api.telegram.org/bot{0}/{1}' \
              .format(self._token, _method)

        print "URL composed:", url
        # print "With data: ", kw
        request = requests.Session()

        if kw:
            data = bytes(_encode_json(kw), 'utf-8')
            # print "Sending over this data: {}".format(data)
            msg = request.post(url, headers={'content-type': 'application/json'}, data=data)
        else:
            msg = request.post(url)

        return _parse_json(msg.text.decode('utf-8'))
