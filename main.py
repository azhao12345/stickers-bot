# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import requests
from requests_toolbelt.adapters import appengine

appengine.monkeypatch()

import logging
from google.appengine.ext import ndb
from google.appengine.api import taskqueue
from google.appengine.api import images
from flask import Flask
from flask import request
from telegram import Telegram

import os
import cloudstorage as gcs

from google.appengine.api import app_identity
import flask.json as fjson
import random

bucket_name = 'trashonlybucket'
tg = Telegram(access_token="548295241:AAFXnRz2zIQu0MJZNuKKdsTf-kIpCfICCac")

app = Flask(__name__)
app.config['DEBUG'] = True

class Pack(ndb.Model):
    gotten = ndb.BooleanProperty()

class Emoji(ndb.Model):
    sticks = ndb.StringProperty(repeated=True) #file ids

@app.route('/')
def hello_world():
    return 'hello world'


@app.route('/telegram', methods=['POST'])
def telegram():
    message = request.get_json()['message']
    if 'sticker' in message:
        print message['sticker']
        print 'set', message['sticker']['set_name']
        key = ndb.Key('Pack', message['sticker']['set_name'])
        if key.get() is None:
            Pack(gotten=True, key=key).put()
        else:
            #pass
            print 'already added'
            return 'already added'
        # now enqueue the task
        task = taskqueue.add(url='/stick', target='sticker', params={'set_name':  message['sticker']['set_name']})
        print task
    return ''

@app.route('/stick', methods=['POST'])
def stick():
    set_name = request.form.get('set_name')
    print set_name
    sticks = tg.getStickerSet(name=set_name)['result']['stickers']
    for stick in sticks:
        # do the thing
        fileid = stick['file_id']
        emoji = stick['emoji']
        print emoji.encode('utf-8')
        emoji = emoji.replace(u'\ufe0f', '')
        print emoji.encode('utf-8')
        # get the file
        fileobj = tg.getFile(file_id=fileid)['result']
        print fileobj
        url = 'https://api.telegram.org/file/bot548295241:AAFXnRz2zIQu0MJZNuKKdsTf-kIpCfICCac/' + fileobj['file_path']
        r = requests.get(url, timeout=60)
        transcoded = transcode(r.content)
        save(fileid + '.png', transcoded)
        saveEmoji(emoji, fileid)
    return ''

@ndb.transactional
def saveEmoji(emoji, fileid):
    # save the emoji here
    key = ndb.Key('Emoji', emoji)
    ent = key.get()
    if key.get() is None:
        ent = Emoji(key=key, sticks=[])
    if fileid not in ent.sticks:
        ent.sticks.append(fileid)
    ent.put()


def transcode(orig_sticker):
    img = images.Image(orig_sticker)
    img.im_feeling_lucky()
    return img.execute_transforms(output_encoding=images.PNG)

def save(filename, data):
    write_retry_params = gcs.RetryParams(backoff_factor=1.1)
    gcs_file = gcs.open('/' + bucket_name + '/' + filename,
                        'w',
                        content_type='image/png',
                        retry_params=write_retry_params)
    gcs_file.write(data)
    gcs_file.close()


@app.route('/chat', methods=['POST'])
def home_post():
    """Respond to POST requests to this endpoint.
    All requests sent to this endpoint from Hangouts Chat are POST
    requests.
    """

    data = request.get_json()

    resp = None

    if data['type'] == 'REMOVED_FROM_SPACE':
        logging.info('Bot removed from a space')

    else:
        resp_dict = format_response(data)
        resp = fjson.jsonify(resp_dict)

    return resp

def format_response(event):
    """Determine what response to provide based upon event data.
    Args:
      event: A dictionary with the event data.
    """

    text = ""

    # Case 1: The bot was added to a room
    if event['type'] == 'ADDED_TO_SPACE' and event['space']['type'] == 'ROOM':
        text = 'Thanks for adding me to "%s"!' % event['space']['displayName']

    # Case 2: The bot was added to a DM
    elif event['type'] == 'ADDED_TO_SPACE' and event['space']['type'] == 'DM':
        text = 'Thanks for adding me to a DM, %s!' % event['user']['displayName']

    elif event['type'] == 'MESSAGE':
        text = event['message']['text']
        text = text.replace('@stickers', '')
        text = text.replace(' ', '')
        key = ndb.Key('Emoji', text.encode('utf-8'))
        print key
        ent = key.get()
        if ent is not None:
            # we are doin it
            sticks = ent.sticks
            result = random.choice(sticks)
            url = 'https://storage.googleapis.com/trashonlybucket/' + result + '.png'
            print result
            print {'text': text, 'previewText': url }
            #return {'text': text, 'previewText': url }
            return {
                'cards': [
                    {
#                        'header': {
#                            'title': text,
#                        },
                        'sections': [
                            {
                                'widgets': [
                                    {
                                        'image': {
                                            'imageUrl': url
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        else:
            return {'text': 'must send an emoji'}

    return { 'text': text }
