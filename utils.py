import json
from glob import glob
from ast import literal_eval
import urllib


def rarity_stats(rarity):
    RARITY = {
        'NORMAL': [1000, 0, 0, 0],
        'RARE': [1050, 100, 0, 0],
        'SUPER RARE': [1100, 120, 0, 0],
        'SUPER SUPER RARE': [1150, 140, 0, 0],
        'ULTRA RARE': [1200, 160, 0, 0]
    }
    return RARITY[rarity.upper()]

def convert_rarity(rarity):
    RARITY = {
        'NORMAL': 0,
        'RARE': 1,
        'SUPER RARE': 2,
        'SUPER SUPER RARE': 3,
        'ULTRA RARE': 4
    }
    return RARITY[rarity.upper()]

def get_cards():
    cards = []
    for card_file in glob('cards/*'):
        d = json.load(open(card_file))
        cards.append(d)
    return cards

def bytes(string, encoding=None):
    return str(string)

def _encode_json(obj):
    print "enconding this object:", obj
    '''
    Encode object as json str.
    '''
    def _dump_obj(obj):
        if isinstance(obj, dict):
            return obj
        else:
            return obj.to_json()
        # d = dict()
        # for k in dir(obj):
        #     if not k.startswith('_'):
        #         d[k] = getattr(obj, k)
        # return d
    return json.dumps(obj, default=_dump_obj)

def _parse_json(jsonstr):
    def _obj_hook(pairs):
        o = JsonObject()
        for k, v in pairs.items():
            o[str(k)] = v
        return o
    return json.loads(jsonstr, object_hook=_obj_hook)

def is_string(var):
    return isinstance(var, basestring)


class JsonObject(dict):
    '''
    general json object that can bind any fields but also act as a dict.
    '''
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(r"'Dict' object has no attribute '%s'" % key)

    def __setattr__(self, attr, value):
        self[attr] = value

